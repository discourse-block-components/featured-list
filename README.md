# Block: Featured List

A block component that can be used within a layout framework like Blocks or Bars. Feature topic lists using multiple filters.

## name:

`block-featured-list`

## params

| name   | description                             | default       | value                    |
| ------ | --------------------------------------- | ------------- | ------------------------ |
| title  | block title                             | Featured List | string                   |
| id     | filter by category id                   |               | number                   |
| tag    | filter by tag name                      |               | string                   |
| solved | filter by solved state                  |               | boolean                  |
| order  | filter by list order                    | activity      | string                   |
| filter | filter by list type                     | latest        | string                   |
| count  | limit the length of the list            | 5             | number                   |
| link   | add a link to see all filtered topics   |               | can be relative, as /c/5 |
| class  | additional css class name for the block |               | string                   |

## template

```scss
.block-featured-list {
  &__container {
  }
  &__header {
  }
  &__title {
  }
  &__list-body {
  }
}
```
